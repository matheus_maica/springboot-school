package com.JPASpringProject.JPA.controller;

import com.JPASpringProject.JPA.model.Subject;
import com.JPASpringProject.JPA.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/list")
    public List<Subject> listAllSubject(){
        return subjectService.listAllSubject();
    }

    @PostMapping("/add")
    public ResponseEntity<Subject> addSubject(@RequestBody Subject subject){
        Subject auxSubject = subjectService.addSubject(subject);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxSubject);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Subject> editSubject(@RequestBody Subject subject, @PathVariable Long id){
        Subject auxSubject = subjectService.editSubject(subject,id);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxSubject);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSubject(@PathVariable Long id){
        subjectService.deleteSubject(id);
    }

    @PutMapping("/join/{student_id}/{subject_id}")
    public ResponseEntity<Subject> joinStudentToSubject(
                                                        @PathVariable Long student_id,
                                                        @PathVariable Long subject_id){

        Subject subject = subjectService.joinStudentToSubject(student_id, subject_id);
        return ResponseEntity.status(HttpStatus.CREATED).body(subject);
    }
}
