package com.JPASpringProject.JPA.controller;

import com.JPASpringProject.JPA.model.Student;
import com.JPASpringProject.JPA.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/list")
    public List<Student> listAllStudent(){
        return studentService.listAllStudent();
    }

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable Long id){
        return studentService.getStudentById(id);
    }

    @PostMapping("/add")
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        Student auxStudent = studentService.addStudent(student);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxStudent);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Student> editStudent(@PathVariable Long id, @RequestBody Student student){
        Student auxStudent = studentService.editStudent(id,student);
        return ResponseEntity.status(HttpStatus.CREATED).body(auxStudent);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable Long id){
        studentService.deleteStudent(id);
    }
}
