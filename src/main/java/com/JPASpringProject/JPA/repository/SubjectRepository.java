package com.JPASpringProject.JPA.repository;

import com.JPASpringProject.JPA.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject,Long> {
}
