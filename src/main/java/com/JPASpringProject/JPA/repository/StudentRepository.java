package com.JPASpringProject.JPA.repository;

import com.JPASpringProject.JPA.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
