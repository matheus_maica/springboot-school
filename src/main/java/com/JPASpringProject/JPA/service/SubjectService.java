package com.JPASpringProject.JPA.service;

import com.JPASpringProject.JPA.model.Student;
import com.JPASpringProject.JPA.model.Subject;
import com.JPASpringProject.JPA.repository.StudentRepository;
import com.JPASpringProject.JPA.repository.SubjectRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentService studentService;
    public List<Subject> listAllSubject() {
        return subjectRepository.findAll();
    }

    public Subject addSubject(Subject subject) {
        return subjectRepository.save(subject);
    }

    public Subject valideteSubject(Long id) {
        Optional<Subject> student = subjectRepository.findById(id);
        if(student.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return  student.get();
    }

    public Subject editSubject(Subject subject, Long id) {
        Subject auxSubject = valideteSubject(id);
        BeanUtils.copyProperties(subject,auxSubject,"id");
        subjectRepository.save(auxSubject);
        return auxSubject;
    }

    public void deleteSubject(Long id) {
        subjectRepository.deleteById(id);
    }

    public Subject joinStudentToSubject(Long student_id, Long subject_id) {
        Student student = studentService.valideteStudent(student_id);
        Subject subject = valideteSubject(subject_id);
        subject.joinStudent(student);
        subjectRepository.save(subject);
        return subject;
    }
}
