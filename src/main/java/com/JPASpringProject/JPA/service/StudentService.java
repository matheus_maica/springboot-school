package com.JPASpringProject.JPA.service;

import com.JPASpringProject.JPA.model.Student;
import com.JPASpringProject.JPA.model.Subject;
import com.JPASpringProject.JPA.repository.StudentRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public List<Student> listAllStudent() {
        return studentRepository.findAll();
    }

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public Student valideteStudent(Long id) {
        Optional<Student> student = studentRepository.findById(id);
        if(student.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return  student.get();
    }

    public Student editStudent(Long id, Student student) {
        Student auxStudent = valideteStudent(id);
        BeanUtils.copyProperties(student,auxStudent,"id");
        studentRepository.save(auxStudent);
        return auxStudent;
    }

    public void deleteStudent(Long id) {
        Student student =valideteStudent(id);
        student.removeStudentFromSubject();
        studentRepository.deleteById(id);
    }

    public Student getStudentById(Long id) {
       Student student =  valideteStudent(id);
       student.getSubjects().forEach(subject -> student.setAuxSubject(subject.getName()));
       return student;



    }
}
