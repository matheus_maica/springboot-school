package com.JPASpringProject.JPA.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    private transient List<String> auxSubject = new ArrayList<>();

    public void setAuxSubject(String subjectName) {
        this.auxSubject.add(subjectName);
    }

    @JsonBackReference
    @ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "students")
    private List<Subject> subjects = new ArrayList<>();

    public void removeStudentFromSubject(){
        for(Subject subject: this.subjects){
            subject.getStudents().remove(this);
        }
    }


}
